package cmd

import (
	"fmt"
	"log"
	"os"
	"sync"

	bukalapakImport "bitbucket.org/perennialsys/erp_marketplace_import/bukalapak"
	"bitbucket.org/perennialsys/erp_marketplace_import/cmd/config"
	tokopediaImport "bitbucket.org/perennialsys/erp_marketplace_import/tokopedia"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "import [--all | [--channel={marketplace|all} [--syncAll={categories|attributes|variants|all}]]]",
	Short: "Import marketplace data for ERP",
	Long: `Imports marketplace data for ERP when specific flags are set.
This imports the categories, variants & attributes for the marketplace and caches them for use in the ERP.`,
	Run: func(cmd *cobra.Command, args []string) {
		var wg sync.WaitGroup
		if *setAll {
			cfg := importConfig[`all`]
			wg.Add(1)
			go func() {
				defer wg.Done()
				tokopediaImport.ImportMarketplace(&cfg)
			}()
			wg.Add(1)
			go func() {
				defer wg.Done()
				bukalapakImport.ImportMarketplace(&cfg)
			}()
			wg.Wait()
		} else {
			cfg, ok := importConfig[syncAll]
			if !ok {
				log.Fatalln(`Invalid sync option provided`)
			}

			switch channel {
			case `bukalapak`:
				wg.Add(1)
				go func() {
					defer wg.Done()
					bukalapakImport.ImportMarketplace(&cfg)
				}()
				wg.Wait()
			case `tokopedia`:
				wg.Add(1)
				go func() {
					defer wg.Done()
					tokopediaImport.ImportMarketplace(&cfg)
				}()
				wg.Wait()
			case `all`:
				wg.Add(1)
				go func() {
					defer wg.Done()
					tokopediaImport.ImportMarketplace(&cfg)
				}()
				wg.Add(1)
				go func() {
					defer wg.Done()
					bukalapakImport.ImportMarketplace(&cfg)
				}()
				wg.Wait()
			default:
				log.Fatalln("Invalid channel selected")
			}
		}
	},
}

var importConfig = map[string]config.Config{
	`all`: {
		SyncAttributes: true,
		SyncVariants:   true,
		SyncCategories: true,
	},
	`categories`: {
		SyncCategories: true,
	},
	`variants`: {
		SyncVariants: true,
	},
	`attributes`: {
		SyncAttributes: true,
	},
}

var (
	channel string
	syncAll string
	setAll  *bool
)

// initializes the local variables with the flags and their default values
func init() {
	setAll = rootCmd.PersistentFlags().Bool("all", false, "imports all marketplaces")
	rootCmd.PersistentFlags().StringVar(&syncAll, "sync", "all", "specify what to syncAll/import[categories|attributes|variants|all]")
	rootCmd.PersistentFlags().StringVar(&channel, "channel", "all", "specify which marketplace to syncAll/import[bukalapak|tokopedia|all]")
}

// Execute the Run function of the command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
