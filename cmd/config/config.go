package config

type Config struct {
	SyncAttributes bool
	SyncVariants   bool
	SyncCategories bool
}
