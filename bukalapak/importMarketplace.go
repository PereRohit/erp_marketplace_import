package bukalapak

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/perennialsys/erp_database/connection/caching"
	"bitbucket.org/perennialsys/erp_database/connection/model"
	"bitbucket.org/perennialsys/erp_marketplace_import/cmd/config"
	"bitbucket.org/perennialsys/thirdparty_api/bukalapak"
	"bitbucket.org/perennialsys/thirdparty_api/bukalapak/auth"
	"bitbucket.org/perennialsys/thirdparty_api/bukalapak/clients"
	bukalapakModel "bitbucket.org/perennialsys/thirdparty_api/bukalapak/model"
	"github.com/go-redis/redis/v8"
	_ "github.com/joho/godotenv/autoload"
)

const (
	categoryKey  = `categories/bukalapak`
	variantKey   = `variants/bukalapak/`
	attributeKey = `attributes/bukalapak/`
)

type Variant struct {
	VariantID      string `json:"variant_id"`
	Name           string `json:"name"`
	DefaultValueID string `json:"default_value_id"`
	IsLimited      bool   `json:"is_limited"`
	Values         []struct {
		ValueID string `json:"value_id"`
		Name    string `json:"name"`
	} `json:"values"`
}

var (
	cacheCon        *redis.Client
	bukalapakClient clients.API
)

func init() {
	var err error
	cacheCon, err = caching.GetConnection(&model.CacheConnectionMeta{Host: os.Getenv("CACHE_HOST_URL")})
	if err != nil {
		log.Fatalln("Unable to obtain connection to cache:", err)
	}

	bukalapakConfig := bukalapak.BukalapakConfig{
		AuthUrl:      os.Getenv("BUKALAPAK_AUTH_URL"),
		ApiUrl:       os.Getenv("BUKALAPAK_API_URL"),
		ClientId:     os.Getenv("BUKALAPAK_CLIENT_ID"),
		ClientSecret: os.Getenv("BUKALAPAK_CLIENT_SECRET"),
		Timeout:      time.Minute,
		Retries:      3,
	}
	apiSession, err := auth.GetPublicToken(bukalapakConfig)
	if err != nil {
		log.Fatalln("Unable to get Bukalapak access token:", err)
	}

	bukalapakClient = clients.NewClient(&bukalapakConfig, apiSession)
}

// ImportMarketplace imports the categories, variants & attributes of the categories
// as specified by the config.
// Multiple goroutines are triggered to process the data retrieved from the API calls
// to put the data into the cache for later use. Only variants & attributes of the leaf
// categories(categories at the end of the category tree) are taken. If the data is not
// found, the category ids are skipped and not inserted.
func ImportMarketplace(cfg *config.Config) {
	defer func(startTime time.Time) {
		log.Println("Bukalapak import took:", time.Now().Sub(startTime))
	}(time.Now())

	categories, err := bukalapakClient.GetAllCategories()
	if err != nil || categories.Meta.HTTPStatus != http.StatusOK {
		log.Fatalln("Unable to get Bukalapak categories:", err)
	}

	var wg sync.WaitGroup

	// populate categories if specified in the configuration
	if cfg.SyncCategories {
		wg.Add(1)
		go func() {
			defer wg.Done()
			catData, err := json.Marshal(categories.Data)
			if err != nil {
				log.Fatalln("Unable to marshal Category data:", err)
			}
			if err := cacheCon.Set(context.Background(), categoryKey, catData, 0).Err(); err != nil {
				log.Fatalln("Unable to set master category data in cache:", err)
			}
		}()
	}
	getLeafChild(categories.Data)

	noVariantsCount := 0
	noAttributesCount := 0

	log.Println("BUKALAPAK: Total categories to fetch variants/attributes:", len(childCats))
	for _, cat := range childCats {
		// populate variants if specified in the configuration
		if cfg.SyncVariants {
			variants, err := bukalapakClient.GetCategoryVariants(cat.ID)
			if err != nil || variants.Meta.HTTPStatus != http.StatusOK {
				log.Fatalln("Unable to get Bukalapak variants :: error:", err, "response error code:", variants.Meta.HTTPStatus)
			}

			// trigger goroutine to populate variants in cache if the category has any
			if len(variants.Data) > 0 {
				wg.Add(1)
				go func(catID int, variants []bukalapakModel.VariantData) {
					defer wg.Done()
					var categoryVariants []Variant
					for _, variant := range variants {
						categoryVariants = append(categoryVariants, Variant{
							VariantID:      strconv.Itoa(variant.ID),
							Name:           variant.Name,
							DefaultValueID: strconv.Itoa(0),
							IsLimited:      false,
							Values: func(varValues []bukalapakModel.Values) (values []struct {
								ValueID string `json:"value_id"`
								Name    string `json:"name"`
							}) {
								for _, varValue := range varValues {
									values = append(values, struct {
										ValueID string `json:"value_id"`
										Name    string `json:"name"`
									}{
										ValueID: strconv.Itoa(varValue.ID),
										Name:    varValue.Name,
									})
								}
								return
							}(variant.Values),
						})
					}
					variantsData, err := json.Marshal(categoryVariants)
					if err != nil {
						log.Fatalln("Unable to marshal variant data:", err)
					}
					if err := cacheCon.Set(context.Background(), variantKey+strconv.Itoa(catID), variantsData, 0).Err(); err != nil {
						log.Fatalln("Unable to set master variants data in cache:", err)
					}
				}(cat.ID, variants.Data)
			} else {
				noVariantsCount++
			}
		}

		// populate attributes if specified in the configuration
		if cfg.SyncAttributes {
			attributes, err := bukalapakClient.GetCategoryAttributes(cat.ID)
			if err != nil || attributes.Meta.HTTPStatus != http.StatusOK {
				log.Fatalln("Unable to get Bukalapak attributes :: error:", err, "response error code:", attributes.Meta.HTTPStatus)
			}
			if len(attributes.Data) > 0 {
				wg.Add(1)
				go func(catID int, attributes []bukalapakModel.AttributesData) {
					defer wg.Done()
					attributeData, err := json.Marshal(attributes)
					if err != nil {
						log.Fatalln("Unable to marshal attribute data:", err)
					}
					if err := cacheCon.Set(context.Background(), attributeKey+strconv.Itoa(catID), attributeData, 0).Err(); err != nil {
						log.Fatalln("Unable to set master attribute data in cache:", err)
					}
				}(cat.ID, attributes.Data)
			} else {
				noAttributesCount++
			}
		}
	}

	log.Println("Waiting for Bukalapak goroutines to complete:", runtime.NumGoroutine())
	wg.Wait()

	log.Println("BUKALAPAK: Categories without variants:", noVariantsCount)
	log.Println("BUKALAPAK: Categories without attributes:", noAttributesCount)
}

var childCats []bukalapakModel.CategoryData

// getLeafChild iterates recursively to reach the end of the category tree
// and adds the leaf category to a  list, using which the variants & attributes
// are fetched.
func getLeafChild(categories []bukalapakModel.CategoryData) {
	for _, cat := range categories {
		if len(cat.Children) != 0 {
			getLeafChild(cat.Children)
		} else {
			childCats = append(childCats, cat)
		}
	}
}
