package tokopedia

import (
	"context"
	"encoding/json"
	"log"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/perennialsys/erp_database/connection/caching"
	"bitbucket.org/perennialsys/erp_database/connection/model"
	"bitbucket.org/perennialsys/erp_marketplace_import/cmd/config"
	"bitbucket.org/perennialsys/thirdparty_api/tokopedia"
	"bitbucket.org/perennialsys/thirdparty_api/tokopedia/auth"
	"bitbucket.org/perennialsys/thirdparty_api/tokopedia/clients"
	tokopediaModel "bitbucket.org/perennialsys/thirdparty_api/tokopedia/model"
	tokopediaProductModel "bitbucket.org/perennialsys/thirdparty_api/tokopedia/model/productmodel"
	"github.com/go-redis/redis/v8"
	_ "github.com/joho/godotenv/autoload"
)

const (
	categoryKey          = `categories/tokopedia`
	variantKey           = `variants/tokopedia/`
	maxRetryCountAllowed = 3
)

type Variant struct {
	VariantID      string `json:"variant_id"`
	Name           string `json:"name"`
	DefaultValueID string `json:"default_value_id"`
	IsLimited      bool   `json:"is_limited"`
	Values         []struct {
		ValueID string `json:"value_id"`
		Name    string `json:"name"`
	} `json:"values"`
}

var (
	cacheCon             *redis.Client
	tokopediaClient      *clients.ApiClient
	fulfillmentServiceID int
)

func init() {
	var err error
	cacheCon, err = caching.GetConnection(&model.CacheConnectionMeta{Host: os.Getenv("CACHE_HOST_URL")})
	if err != nil {
		log.Fatalln("Unable to obtain connection to cache:", err)
	}

	apiSession, err := auth.GetAuthToken(tokopedia.TokopediaConfig{
		AuthUrl:      os.Getenv("TOKOPEDIA_AUTH_URL"),
		ClientId:     os.Getenv("TOKOPEDIA_CLIENT_ID"),
		ClientSecret: os.Getenv("TOKOPEDIA_CLIENT_SECRET"),
		Timeout:      time.Minute,
	})
	if err != nil {
		log.Fatalln("Unable to get Tokopedia access token:", err)
	}

	tokopediaClient = clients.NewClient(os.Getenv("TOKOPEDIA_API_URL"), apiSession.AccessToken)
	fulfillmentServiceID, err = strconv.Atoi(os.Getenv("TOKOPEDIA_APP_ID"))
	if err != nil {
		log.Fatalln("Unable to obtain Tokopedia fulfillment service ID(AppID):", err)
	}
}

// ImportMarketplace imports the categories & variants of the categories as specified by the config.
// Multiple goroutines are triggered to process the data retrieved from the API calls
// to put the data into the cache for later use. Only variants of the leaf
// categories(categories at the end of the category tree) are taken. If the data is not
// found, the category ids are skipped and not inserted.
// Tokopedia API calls are not able to return consistent data. Sometimes, the Header as well
// as the Data field is empty. The workaround for this situation is to store the incorrectly
// obtained category IDs and try to fetch them again. If the same retry count is persistent,
// they are skipped altogether and displayed back to be debugged/manually inserted.
func ImportMarketplace(cfg *config.Config) {
	defer func(startTime time.Time) {
		log.Println("Tokopedia import took:", time.Now().Sub(startTime))
	}(time.Now())

	categories, err := tokopediaClient.GetAllCategories(fulfillmentServiceID, "")
	if err != nil || categories.Header.ErrorCode != 0 {
		log.Fatalln("Unable to get Tokopedia categories:", err)
	}

	var wg sync.WaitGroup

	// populate categories if specified in the configuration
	if cfg.SyncCategories {
		wg.Add(1)
		go func() {
			defer wg.Done()
			catData, err := json.Marshal(categories.Data.Categories)
			if err != nil {
				log.Fatalln("Unable to marshal Category data:", err)
			}
			if err := cacheCon.Set(context.Background(), categoryKey, catData, 0).Err(); err != nil {
				log.Fatalln("Unable to set master category data in cache:", err)
			}
		}()
	}
	getLeafChild(categories.Data.Categories)

	noVariantsCount := 0

	remainingCategoryCount := make([]int, maxRetryCountAllowed)

	// populate variants iff specified in the configuration
	for cfg.SyncVariants && len(childCats) > 0 {
		var tempChildCats []tokopediaModel.Categories

		// re-acquire auth token as it expires after a certain time
		apiSession, err := auth.GetAuthToken(tokopedia.TokopediaConfig{
			AuthUrl:      os.Getenv("TOKOPEDIA_AUTH_URL"),
			ClientId:     os.Getenv("TOKOPEDIA_CLIENT_ID"),
			ClientSecret: os.Getenv("TOKOPEDIA_CLIENT_SECRET"),
			Timeout:      time.Minute,
		})
		if err != nil {
			log.Fatalln("Unable to get Tokopedia access token:", err)
		}
		tokopediaClient := clients.NewClient(os.Getenv("TOKOPEDIA_API_URL"), apiSession.AccessToken)

		log.Println("TOKOPEDIA: Total categories to fetch variants:", len(childCats))
		for _, cat := range childCats {
			variants, err := tokopediaClient.GetVariantsByCategoryId(fulfillmentServiceID, cat.ID)
			if err != nil || variants.Header.ErrorCode != "" {
				log.Fatalln("Unable to get Tokopedia variants :: error:", err, "response error code:", variants.Header.ErrorCode)
			}

			// skip processing the data if not present
			if len(variants.Data) == 0 {
				// if variants for the ID is not retrieved properly from API, retry later
				if variants.Header.ProcessTime == 0 && variants.Header.Reason == "" {
					tempChildCats = append(tempChildCats, cat)
				} else {
					// for other cases where the category has no variants
					noVariantsCount++
				}
				continue
			}

			wg.Add(1)
			go func(catID string, variants []tokopediaProductModel.VariantByCategoryData) {
				// only 1117 variant keys are set
				// IDs not fetched: 3167, 2710 (something wrong)
				// IDs without variants: 2570, 2216, 2205
				// working with last update(58m13.85576326s) but IDs without variants also added [2664 added]
				// all 2661 added(excluding 3 cats without variants) [working but time taken approx. 1hr]
				defer wg.Done()
				var categoryVariants []Variant
				for _, variant := range variants {
					categoryVariants = append(categoryVariants, Variant{
						VariantID:      strconv.Itoa(variant.VariantID),
						Name:           variant.Name,
						DefaultValueID: strconv.Itoa(variant.Units[0].UnitID),
						IsLimited:      true,
						Values: func(varValues []tokopediaProductModel.UnitValues) (values []struct {
							ValueID string `json:"value_id"`
							Name    string `json:"name"`
						}) {
							for _, varValue := range varValues {
								values = append(values, struct {
									ValueID string `json:"value_id"`
									Name    string `json:"name"`
								}{
									ValueID: strconv.Itoa(varValue.ValueID),
									Name:    varValue.Value,
								})
							}
							return
						}(variant.Units[0].Values),
					})
				}
				variantsData, err := json.Marshal(categoryVariants)
				if err != nil {
					log.Fatalln("Unable to marshal variant data:", err)
				}
				if err := cacheCon.Set(context.Background(), variantKey+catID, variantsData, 0).Err(); err != nil {
					log.Fatalln("Unable to set master variants data in cache:", err)
				}
			}(cat.ID, variants.Data)
		}
		remainingCategoryCount = append(remainingCategoryCount[1:], len(tempChildCats))
		if remainingCategoryCount[0] == remainingCategoryCount[len(remainingCategoryCount)-1] && len(remainingCategoryCount) != 0 {
			log.Printf("Unable to fetch variants for following categories: %+v\n", tempChildCats)
			break
		}
		childCats = tempChildCats
	}

	log.Println("Waiting for Tokopedia goroutines to complete:", runtime.NumGoroutine())
	wg.Wait()

	log.Println("TOKOPEDIA: Categories without variants:", noVariantsCount)
}

var childCats []tokopediaModel.Categories

// getLeafChild iterates recursively to reach the end of the category tree
// and adds the leaf category to a  list, using which the variants
// are fetched.
func getLeafChild(categories []tokopediaModel.Categories) {
	for _, cat := range categories {
		if len(cat.Child) != 0 {
			getLeafChild(cat.Child)
		} else {
			childCats = append(childCats, cat)
		}
	}
}
