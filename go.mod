module bitbucket.org/perennialsys/erp_marketplace_import

go 1.14

require (
	bitbucket.org/perennialsys/erp_database v0.0.0-20210305125407-e955027d3027
	bitbucket.org/perennialsys/thirdparty_api v0.0.0-20210305130428-60e19be7334a
	github.com/go-redis/redis/v8 v8.7.1
	github.com/go-resty/resty/v2 v2.5.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/spf13/cobra v1.1.3
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83 // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/sys v0.0.0-20210305230114-8fe3ee5dd75b // indirect
)
