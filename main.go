package main

import (
	"bitbucket.org/perennialsys/erp_marketplace_import/cmd"
)

func main() {
	cmd.Execute()
}
